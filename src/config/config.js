const dotenv = require("dotenv");

// configure the env variable
dotenv.config();
module.exports = {
  PORT: process.env.PORT,
};
