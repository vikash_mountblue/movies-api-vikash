const dataBase = require("./config/databaseConfig");
const movieData = require("./data/movies-api.json");

// Drop table if exist

const dropTable = (tableName) =>
  new Promise((resolve, reject) => {
    const sqlquery = `DROP TABLE IF EXISTS ${tableName}`;
    dataBase.query(sqlquery, (err, res) => {
      if (err) reject(err);
      resolve(res);
    });
  });

// Create Director table

const createDirectorTable = () => {
  const sqlquery =
    "CREATE TABLE directors(Id INT NOT NULL PRIMARY KEY AUTO_INCREMENT , Director_Name VARCHAR(50))";
  return new Promise((resolve, reject) => {
    dataBase.query(sqlquery, (err, res) => {
      if (err) reject(err);
      resolve(res);
    });
  });
};

// Fetch all unique Director name from movies-api json data

const uniqueDirector = () => {
  const director = new Set();
  movieData.forEach((data) => {
    director.add(data.Director);
  });
  const directorArray = [];
  director.forEach((item) => {
    directorArray.push({
      Director: item,
    });
  });
  return directorArray;
};

// Insert director data into director table

const insertDataIntoDirectorTable = () => {
  const uniqueDirectorName = uniqueDirector();
  const promise = uniqueDirectorName.map(
    (element) =>
      new Promise((resolve, reject) => {
        const sqlquery = `insert into directors (Director_Name) VALUES ("${element.Director}");`;
        dataBase.query(sqlquery, (error, res) => {
          if (error) reject(error);
          resolve(res);
        });
      })
  );
  Promise.all(promise);
};

// Create Movies table

const createMovieTable = () => {
  const sqlquery =
    "CREATE TABLE movies(movie_id int NOT NULL PRIMARY KEY AUTO_INCREMENT, Ranks int NOT NULL, Title varchar(50), Description varchar(255), Runtime int, Genre varchar(10), Rating float, Metascore varchar(10), Votes int, Gross_Earning_in_Mil varchar(10), Actor varchar(50), Year int, Director_id INT NOT NULL , FOREIGN KEY(Director_id) REFERENCES directors(Id) ON DELETE CASCADE ON UPDATE CASCADE)";
  return new Promise((resolve, reject) => {
    dataBase.query(sqlquery, (err, res) => {
      if (err) reject(err);
      resolve(res);
    });
  });
};

// Fetch Director ID from director table

const getDirectorID = (name) => {
  const sqlquery_getId = `SELECT * FROM directors WHERE Director_Name = "${name}"`;
  return new Promise((resolve, reject) => {
    dataBase.query(sqlquery_getId, (err, res) => {
      if (err) reject(err);
      resolve(res[0].Id);
    });
  });
};

// Insert movies data into movies table and director_id from director table

const insertDataIntoMoviesTable = (data) => {
  const promise = data.map(
    (element) =>
      new Promise((resolve, reject) => {
        const directname = getDirectorID(element.Director);
        directname.then((val) => {
          const sqlquery = `INSERT INTO movies(Ranks, Title, Description, Runtime, Genre, Rating, Metascore, Votes, Gross_Earning_in_Mil, Actor, Year, Director_id) values(${element.Rank}, "${element.Title}", "${element.Description}", ${element.Runtime}, "${element.Genre}", ${element.Rating}, "${element.Metascore}", ${element.Votes}, "${element.Gross_Earning_in_Mil}", "${element.Actor}", ${element.Year}, ${val})`;
          dataBase.query(sqlquery, (err, res) => {
            if (err) reject(err);
            resolve(res);
          });
        });
      })
  );
  Promise.all(promise);
};

const main = async () => {
  await dropTable("movies");
  await dropTable("directors");
  await createDirectorTable();
  await insertDataIntoDirectorTable();
  await createMovieTable();
  await insertDataIntoMoviesTable(movieData);
};

module.exports = main();
