const express = require("express");

const { PORT } = require("./src/config/config");
const dataBase = require("./src/config/databaseConfig");
const seedData = require("./src/seedMovies-api");

const app = express();
dataBase.connect((err) => {
  if (err) throw err;
  console.log(" Database Connected!");
});

seedData;

app.listen(PORT, () => console.log(`Listening on port: ${PORT}`));
